DECLARE @SQLString NVARCHAR(4000)=N'
DECLARE @ProjectInfo NVARCHAR(3750) 
SELECT @ProjectInfo = (
  SELECT * FROM (Select
    SYSTEM_USER, @Version, @Project, @Description, db_name(), 
    App_Name(), GetDate()
  )f(SystemUser, version,Project,[Description], [Database], [Application], Created) FOR JSON auto );
IF not EXISTS
  (SELECT name, value  FROM fn_listextendedproperty(
     N''Project_Info'',default, default, default, default, default, default) )
    EXEC sys.sp_addextendedproperty @name=N''Project_Info'', @value=@ProjectInfo
ELSE
  EXEC sys.sp_Updateextendedproperty  @name=N''Project_Info'', @value=@ProjectInfo'
DECLARE @Parameters NVARCHAR(200)='@Version NVARCHAR(10), @Project NVARCHAR(80), @Description NVARCHAR(800)'
EXECUTE sp_ExecuteSQL @SQLString,@Parameters, @Version='1.34',@Project='Pubs',@Description='a tryout of SCA documentation' 
