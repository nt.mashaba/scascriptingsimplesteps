SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[reptq1] AS
select pub_id, title_id, price, pubdate
from titles
where price is NOT NULL
order by pub_id

GO
