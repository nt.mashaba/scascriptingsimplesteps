CREATE TABLE [dbo].[pub_info]
(
[pub_id] [char] (4) COLLATE Latin1_General_CI_AS NOT NULL,
[logo] [image] NULL,
[pr_info] [text] COLLATE Latin1_General_CI_AS NULL,
[Pub_Info] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[pub_info] ADD CONSTRAINT [PK_pub_info] PRIMARY KEY CLUSTERED  ([Pub_Info])
GO
ALTER TABLE [dbo].[pub_info] ADD FOREIGN KEY ([pub_id]) REFERENCES [dbo].[publishers] ([pub_id])
GO
