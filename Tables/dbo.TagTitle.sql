CREATE TABLE [dbo].[TagTitle]
(
[TagTitle_ID] [int] NOT NULL IDENTITY(1, 1),
[title_id] [dbo].[tid] NOT NULL,
[TagName_ID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[TagTitle] ADD CONSTRAINT [PK_TagTitle] PRIMARY KEY CLUSTERED  ([title_id], [TagName_ID])
GO
CREATE NONCLUSTERED INDEX [idxTagName_ID] ON [dbo].[TagTitle] ([TagName_ID]) INCLUDE ([TagTitle_ID], [title_id])
GO
ALTER TABLE [dbo].[TagTitle] ADD FOREIGN KEY ([TagName_ID]) REFERENCES [dbo].[TagName] ([TagName_ID])
GO
ALTER TABLE [dbo].[TagTitle] ADD FOREIGN KEY ([title_id]) REFERENCES [dbo].[titles] ([title_id])
GO
