SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create function [dbo].[StripXMLTags]( @xml XML ) returns varchar(max) as
begin
    declare @TextWithoutTags varchar(max);
    with TheXMLDocument(contents) as
    (
        select TheXML.node.query('.') from @XML.nodes('/') as TheXML(node)
    )
    select @TextWithoutTags = contents.value('.', 'varchar(max)') from TheXMLDocument
    return @TextWithoutTags
end
GO
